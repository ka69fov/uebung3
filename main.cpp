#include <iostream>
#include <vector>
#include <cstring>
#include <sstream>
#include <string>
#include <algorithm>

bool checkSubsequence(std::vector<std::string> X, std::vector<std::string> Y, int m, int n)
{
    int i=0;

    while(Y.size()>i)
    {
        std::string a = X[i];
        std::string b = Y[i];

        if(X[i].compare(Y[i])==0)
        {
            if(i==X.size()-1)
            {
                return true;
            }
            else
            {
                i++;
            }

        }
        else
        {
            Y.erase(Y.begin());
        }
    }

    return false;
}

void printLCS(std::vector<std::string> inputVector)
{
    std::cout << "LCS: ";
    for (int i=0; i<inputVector.size(); i++)
    {
        std::cout << inputVector[i];
    }
}

void buildSubSequences(std::vector<std::vector<std::string> > *subSeqXlist, std::vector<std::string> X, int m)
{
    subSeqXlist->push_back(X);

    for(int i=0; i<m; i++)
    {
        std::vector<std::string> newSeqX = X;
        newSeqX.erase(newSeqX.begin() + i);

        if(m>1)
        {
            buildSubSequences(subSeqXlist, newSeqX, m-1);
        }
    }
}

std::vector<std::string> findPrimitiveLCS(std::vector<std::vector<std::string> > subSeqXlist, std::vector<std::string> Y, int n)
{
    int k=subSeqXlist.size();
    std::vector<std::string> currLCS;
    int currLCSlength = 0;

    for(int i=0; i<k; i++)
    {
        std::vector<std::string> currSubSeq = subSeqXlist[i];
        int currSubSeqSize = currSubSeq.size();

        if(currSubSeqSize>currLCSlength)
        {
            if(checkSubsequence(currSubSeq, Y, currSubSeqSize, n)==true)
            {
                currLCS=currSubSeq;
                currLCSlength=currSubSeqSize;
            }
        }
    }

    return currLCS;
}

void findLCS(std::vector<std::string> X, std::vector<std::string> Y, int m, int n)
{
    int matrix[m+1][n+1];

    for(int i=0; i<m+1; i++)
    {
        for(int j=0; j<n+1; j++)
        {
            matrix[i][j] = 0;
        }
    }

    for(int i=1; i<=m; i++)
    {
        for(int j=1; j<=n; j++)
        {
            if(X[i-1].compare(Y[j-1])==0)
            {
                matrix[i][j]=matrix[i-1][j-1]+1;
            }
            else
            {
                matrix[i][j]=std::max(matrix[i-1][j], matrix[i][j-1]);
            }
        }
    }

    std::cout << "Length of LCS:" << matrix[m][n] << std::endl;

    int i=m;
    int j=n;
    std::string LCS = "";

    while(i>0 || j>0)
    {
        if(matrix[i][j]>matrix[i-1][j] && matrix[i][j]>matrix[i][j-1])
        {
            LCS.append(X[i-1]);
            i--;
            j--;
        }
        else if(matrix[i-1][j]>matrix[i][j-1])
        {
            i--;
        }
        else
        {
            j--;
        }
    }

    std::reverse(LCS.begin(), LCS.end());

    std::cout << "LCS: " << LCS << std::endl;
}

int main(int argc, char** argv)
{
    //Read console input
    if (3 > argc) {
        std::cout << "usage: " << argv[0] << " <integer>" << std::endl;
        return -1;
    }
    std::string mode;
    std::string stringX = argv[1];
    std::string stringY = argv[2];

    if (4 == argc) {
        mode = "naive";
    }

/*
    //std::string stringX = "ABCBDAB";
    //std::string stringY = "BDCABA";
    std::string stringX = "ABCDAF";
    std::string stringY = "ACBCF";
*/
    //Initialize Vectors
    int m=stringX.length();
    int n=stringY.length();
    std::vector<std::string> seqX;
    std::vector<std::string> seqY;

    //Transform Strings into Vectors
    for(int i=0; i<m; i++)
    {
        seqX.push_back(stringX.substr(i,1));
    }
    for(int i=0; i<n; i++)
    {
        seqY.push_back(stringY.substr(i,1));
    }

    if(mode.compare("naive")==0)
    {
        std::vector<std::vector<std::string> > subSeqXlist;
        buildSubSequences(&subSeqXlist, seqX, m);
        std::vector<std::string> LCS = findPrimitiveLCS(subSeqXlist, seqY, m);
        printLCS(LCS);
    }
    else
    {
        findLCS(seqX,seqY,m,n);
    }

    return 0;
}

